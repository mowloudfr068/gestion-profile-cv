using GestionProfilCV.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Data;
using System.Data.SqlClient;

namespace GestionProfilCV.Pages.Competences
{
    public class EditModel : PageModel
    {
        public Competence competence = new Competence();
        public string errorMessage = "";
        public string successMessage = "";
        public void OnGet()
        {
            int? idUtilisateur = HttpContext.Session.GetInt32("ID_Utilisateur");
            int ID_Competence = Convert.ToInt32(Request.Query["ID_Competence"]);
            if (idUtilisateur == null)
            {
                return; // Ou redirigez vers une autre page, car aucun ID utilisateur n'est disponible dans la session
            }

            int id = (int)idUtilisateur;
            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Cr�er la commande SQL pour l'insertion
                    string query = "SELECT * FROM Competences where ID_Competence=@ID_Competence AND ID_Utilisateur=@id";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        // Param�tres de la commande
                        command.Parameters.AddWithValue("@ID_Competence", ID_Competence);
                        command.Parameters.AddWithValue("@id", id);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                competence.ID_Utilisateur = id;
                                competence.ID_Competence = reader.GetInt32("ID_Competence");
                                competence.NomCompetence = reader.GetString("NomCompetence");
                                competence.NiveauCompetence = reader.GetString("NiveauCompetence");
                            }
                        }


                    }

                }

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }


        }

        public void OnPost()
        {
            competence.ID_Competence = Convert.ToInt32(Request.Form["ID_Competence"]);
            competence.NomCompetence = Request.Form["NomCompetence"];
            competence.NiveauCompetence = Request.Form["NiveauCompetence"];
            if (string.IsNullOrEmpty(competence.NomCompetence)
                || string.IsNullOrEmpty(competence.NiveauCompetence)
              
            )
            {
                errorMessage = "Tous les champs doivent �tre remplis.";
                return;
            }

            // Le formulaire est valide, proc�der � la cr�ation du compte competence
            // ...

            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    string query = "UPDATE Competences SET NomCompetence = @NomCompetence, NiveauCompetence = @NiveauCompetence WHERE ID_Competence = @Id AND ID_Utilisateur=@ID_Utilisateur";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        command.Parameters.AddWithValue("@NomCompetence", competence.NomCompetence);
                        command.Parameters.AddWithValue("@NiveauCompetence", competence.NiveauCompetence);
                        command.Parameters.AddWithValue("@Id", competence.ID_Competence);
                        command.Parameters.AddWithValue("@ID_Utilisateur", HttpContext.Session.GetInt32("ID_Utilisateur"));
                        command.ExecuteNonQuery();
                        Response.Redirect("/Competences/Index");
                    }
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }

        }
    }
}
