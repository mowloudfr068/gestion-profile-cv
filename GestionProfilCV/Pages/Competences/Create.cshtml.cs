using GestionProfilCV.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Data.SqlClient;

namespace GestionProfilCV.Pages.Competences
{
    public class CreateModel : PageModel
    {
        public string errorMessage = "";
        public string successMessage = "";
        public Competence competence = new Competence();

        public void OnGet()
        {
        }

        public void OnPost()
        {
            int? idUtilisateur = HttpContext.Session.GetInt32("ID_Utilisateur");
            if (idUtilisateur == null)
            {
                return; // Ou redirigez vers une autre page, car aucun ID utilisateur n'est disponible dans la session
            }

            int id = (int)idUtilisateur;
            competence.ID_Utilisateur = id;
            competence.NomCompetence = Request.Form["NomCompetence"];
            competence.NiveauCompetence = Request.Form["NiveauCompetence"];

            if (string.IsNullOrEmpty(competence.NomCompetence)
                || string.IsNullOrEmpty(competence.NiveauCompetence)
              )
            {
                errorMessage = "Tous les champs doivent �tre remplis.";
                return;
            }

            // Le formulaire est valide, proc�der � la cr�ation du compte competence
            // ...

            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Cr�er la commande SQL pour l'insertion
                    string query = "INSERT INTO Competences (ID_Utilisateur, NomCompetence, NiveauCompetence) " +
                                   "VALUES (@ID_Utilisateur, @NomCompetence, @NiveauCompetence)";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        // Param�tres de la commande
                        command.Parameters.AddWithValue("@ID_Utilisateur", competence.ID_Utilisateur);
                        command.Parameters.AddWithValue("@NomCompetence", competence.NomCompetence);
                        command.Parameters.AddWithValue("@NiveauCompetence", competence.NiveauCompetence);

                        // Ex�cuter la commande d'insertion
                        int rowsAffected = command.ExecuteNonQuery();

                        if (rowsAffected > 0)
                        {
                            // L'insertion a r�ussi
                            successMessage = "Votre competence a �t� cr�� avec succ�s !";
                        }
                        else
                        {
                            // L'insertion a �chou�
                            errorMessage = "Une erreur s'est produite lors de la cr�ation du competence.";
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }
            competence.NomCompetence = "";
            competence.NiveauCompetence = "";
            successMessage = "Votre competence a �t� cr�� avec succ�s.";
            Response.Redirect("/Competences/Create");
        }
    }
}
