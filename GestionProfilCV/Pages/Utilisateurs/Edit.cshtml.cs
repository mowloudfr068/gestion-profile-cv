using GestionProfilCV.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Data.SqlClient;

namespace GestionProfilCV.Pages.Utilisateurs
{
    public class EditModel : PageModel
    {
        public Utilisateur utilisateur = new Utilisateur();
        public string errorMessage = "";
        public string successMessage = "";
        public void OnGet()
        {
            int? idUtilisateur = HttpContext.Session.GetInt32("ID_Utilisateur");
            if (idUtilisateur == null)
            {
                return; // Ou redirigez vers une autre page, car aucun ID utilisateur n'est disponible dans la session
            }

            int id = (int)idUtilisateur;
            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Cr�er la commande SQL pour l'insertion
                    string query = "SELECT * FROM Utilisateurs where ID_Utilisateur=@id";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        // Param�tres de la commande
                        command.Parameters.AddWithValue("@id", id);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                utilisateur.ID_Utilisateur = id;
                                utilisateur.Nom = reader.GetString(1);
                                utilisateur.Prenom = reader.GetString(2);
                                utilisateur.AdresseEmail = reader.GetString(3);
                                utilisateur.MotDePasse = reader.GetString(4);
                                utilisateur.DateNaissance = reader.GetDateTime(5);
                            }
                        }

                        
                    }

                }

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }


        }

        public void OnPost()
        {
            utilisateur.Nom = Request.Form["Nom"];
            utilisateur.Prenom = Request.Form["prenom"];
            utilisateur.AdresseEmail = Request.Form["AdresseEmail"];
            utilisateur.MotDePasse = Request.Form["MotDePasse"];
            string dateNaissanceString = Request.Form["DateNaissance"];
            DateTime dateNaissance;
            if (!DateTime.TryParse(dateNaissanceString, out dateNaissance))
            {
                errorMessage = "La date de naissance est invalide.";
                return;
            }
            utilisateur.DateNaissance = dateNaissance;
            if (string.IsNullOrEmpty(utilisateur.Nom)
                || string.IsNullOrEmpty(utilisateur.Prenom)
                || string.IsNullOrEmpty(utilisateur.AdresseEmail)
                || string.IsNullOrEmpty(utilisateur.MotDePasse)
                || utilisateur.DateNaissance == DateTime.MinValue)
            {
                errorMessage = "Tous les champs doivent �tre remplis.";
                return;
            }

            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    string query = "UPDATE Utilisateurs SET Nom = @Nom, Prenom = @Prenom, AdresseEmail = @AdresseEmail, MotDePasse = @MotDePasse, DateNaissance = @DateNaissance WHERE ID_Utilisateur = @Id";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        command.Parameters.AddWithValue("@Nom", utilisateur.Nom);
                        command.Parameters.AddWithValue("@Prenom", utilisateur.Prenom);
                        command.Parameters.AddWithValue("@AdresseEmail", utilisateur.AdresseEmail);
                        command.Parameters.AddWithValue("@MotDePasse", utilisateur.MotDePasse);
                        command.Parameters.AddWithValue("@DateNaissance", utilisateur.DateNaissance);
                        command.Parameters.AddWithValue("@Id", HttpContext.Session.GetInt32("ID_Utilisateur"));
                        command.ExecuteNonQuery();
                        HttpContext.Session.Remove("UtilisateurConnecte");
                        Response.Redirect("/Login");
                    }
                }
            }catch(Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }
            
        }
    }
}
