using GestionProfilCV.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Data;
using System.Data.SqlClient;

namespace GestionProfilCV.Pages.Certifications
{
    public class EditModel : PageModel
    {
        public Certification certification = new Certification();
        public string errorMessage = "";
        public string successMessage = "";
        public void OnGet()
        {
            int? idUtilisateur = HttpContext.Session.GetInt32("ID_Utilisateur");
            int ID_Certification = Convert.ToInt32(Request.Query["ID_Certification"]);
            if (idUtilisateur == null)
            {
                return; // Ou redirigez vers une autre page, car aucun ID utilisateur n'est disponible dans la session
            }

            int id = (int)idUtilisateur;
            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Cr�er la commande SQL pour l'insertion
                    string query = "SELECT * FROM Certifications where ID_Certification=@ID_Certification AND ID_Utilisateur=@id";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        // Param�tres de la commande
                        command.Parameters.AddWithValue("@ID_Certification", ID_Certification);
                        command.Parameters.AddWithValue("@id", id);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                certification.ID_Utilisateur = id;
                                certification.ID_Certification = reader.GetInt32("ID_Certification");
                                certification.NomCertification = reader.GetString("NomCertification");
                                certification.AutoriteCertification = reader.GetString("AutoriteCertification");
                                certification.DateObtention = reader.GetDateTime("DateObtention");
                            }
                        }


                    }

                }

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }


        }

        public void OnPost()
        {
            certification.ID_Certification = Convert.ToInt32(Request.Form["ID_Certification"]);
            certification.NomCertification = Request.Form["NomCertification"];
            certification.AutoriteCertification = Request.Form["AutoriteCertification"];
            string DateObtentionString = Request.Form["DateObtention"];
            DateTime dateObtention;
            if (!DateTime.TryParse(DateObtentionString, out dateObtention))
            {
                errorMessage = "La date de naissance est invalide.";
                return;
            }
            certification.DateObtention = dateObtention;

            if (string.IsNullOrEmpty(certification.NomCertification)
                || string.IsNullOrEmpty(certification.AutoriteCertification)
                || certification.DateObtention == DateTime.MinValue)
            {
                errorMessage = "Tous les champs doivent �tre remplis.";
                return;
            }

            // Le formulaire est valide, proc�der � la cr�ation du compte certification
            // ...

            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    string query = "UPDATE Certifications SET NomCertification = @NomCertification, AutoriteCertification = @AutoriteCertification,DateObtention = @DateObtention WHERE ID_Certification = @Id AND ID_Utilisateur=@ID_Utilisateur";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        command.Parameters.AddWithValue("@NomCertification", certification.NomCertification);
                        command.Parameters.AddWithValue("@AutoriteCertification", certification.AutoriteCertification);
                        command.Parameters.AddWithValue("@DateObtention", certification.DateObtention);
                        command.Parameters.AddWithValue("@Id", certification.ID_Certification);
                        command.Parameters.AddWithValue("@ID_Utilisateur", HttpContext.Session.GetInt32("ID_Utilisateur"));
                        command.ExecuteNonQuery();
                        Response.Redirect("/Certifications/Index");
                    }
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }

        }
    }
}
