using GestionProfilCV.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Data.SqlClient;

namespace GestionProfilCV.Pages.Certifications
{
    public class CreateModel : PageModel
    {
        public string errorMessage = "";
        public string successMessage = "";
        public Certification certification = new Certification();

        public void OnGet()
        {
        }

        public void OnPost()
        {
            int? idUtilisateur = HttpContext.Session.GetInt32("ID_Utilisateur");
            if (idUtilisateur == null)
            {
                return; // Ou redirigez vers une autre page, car aucun ID utilisateur n'est disponible dans la session
            }

            int id = (int)idUtilisateur;
            certification.ID_Utilisateur = id;
            certification.NomCertification = Request.Form["NomCertification"];
            certification.AutoriteCertification = Request.Form["AutoriteCertification"];
            string DateObtentionString = Request.Form["DateObtention"];
            DateTime dateObtention;
            if (!DateTime.TryParse(DateObtentionString, out dateObtention))
            {
                errorMessage = "La date de naissance est invalide.";
                return;
            }
            certification.DateObtention = dateObtention;

            if (string.IsNullOrEmpty(certification.NomCertification)
                || string.IsNullOrEmpty(certification.AutoriteCertification)
                || certification.DateObtention == DateTime.MinValue)
            {
                errorMessage = "Tous les champs doivent �tre remplis.";
                return;
            }

            // Le formulaire est valide, proc�der � la cr�ation du compte certification
            // ...

            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Cr�er la commande SQL pour l'insertion
                    string query = "INSERT INTO Certifications (ID_Utilisateur, NomCertification, AutoriteCertification,DateObtention) " +
                                   "VALUES (@ID_Utilisateur, @NomCertification, @AutoriteCertification, @DateObtention)";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        // Param�tres de la commande
                        command.Parameters.AddWithValue("@ID_Utilisateur", certification.ID_Utilisateur);
                        command.Parameters.AddWithValue("@NomCertification", certification.NomCertification);
                        command.Parameters.AddWithValue("@AutoriteCertification", certification.AutoriteCertification);
                        command.Parameters.AddWithValue("@DateObtention", certification.DateObtention);

                        // Ex�cuter la commande d'insertion
                        int rowsAffected = command.ExecuteNonQuery();

                        if (rowsAffected > 0)
                        {
                            // L'insertion a r�ussi
                            successMessage = "Votre certification a �t� cr�� avec succ�s !";
                        }
                        else
                        {
                            // L'insertion a �chou�
                            errorMessage = "Une erreur s'est produite lors de la cr�ation du certification.";
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }
            certification.NomCertification = "";
            certification.AutoriteCertification = "";
            certification.DateObtention = DateTime.MinValue;
            successMessage = "Votre certification a �t� cr�� avec succ�s.";
            Response.Redirect("/Certifications/Create");
        }
    }
}
