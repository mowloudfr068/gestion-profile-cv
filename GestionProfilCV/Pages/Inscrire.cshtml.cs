using GestionProfilCV.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Data.SqlClient;

namespace GestionProfilCV.Pages
{
    public class InscrireModel : PageModel
    {
        public Utilisateur utilisateur = new Utilisateur();
        public string errorMessage = "";
        public string successMessage = "";

        public void OnGet()
        {
        }

        public void OnPost()
        {
            utilisateur.Nom = Request.Form["Nom"];
            utilisateur.Prenom = Request.Form["prenom"];
            utilisateur.AdresseEmail = Request.Form["AdresseEmail"];
            utilisateur.MotDePasse = Request.Form["MotDePasse"];

            string dateNaissanceString = Request.Form["DateNaissance"];
            DateTime dateNaissance;
            if (!DateTime.TryParse(dateNaissanceString, out dateNaissance))
            {
                errorMessage = "La date de naissance est invalide.";
                return;
            }
            utilisateur.DateNaissance = dateNaissance;

            if (string.IsNullOrEmpty(utilisateur.Nom)
                || string.IsNullOrEmpty(utilisateur.Prenom)
                || string.IsNullOrEmpty(utilisateur.AdresseEmail)
                || string.IsNullOrEmpty(utilisateur.MotDePasse)
                || utilisateur.DateNaissance == DateTime.MinValue)
            {
                errorMessage = "Tous les champs doivent �tre remplis.";
                return;
            }

            // Le formulaire est valide, proc�der � la cr�ation du compte utilisateur
            // ...

            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Cr�er la commande SQL pour l'insertion
                    string query = "INSERT INTO Utilisateurs (Nom, Prenom, AdresseEmail, MotDePasse, DateNaissance) " +
                                   "VALUES (@Nom, @Prenom, @AdresseEmail, @MotDePasse, @DateNaissance)";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        // Param�tres de la commande
                        command.Parameters.AddWithValue("@Nom", utilisateur.Nom);
                        command.Parameters.AddWithValue("@Prenom", utilisateur.Prenom);
                        command.Parameters.AddWithValue("@AdresseEmail", utilisateur.AdresseEmail);
                        command.Parameters.AddWithValue("@MotDePasse", utilisateur.MotDePasse);
                        command.Parameters.AddWithValue("@DateNaissance", utilisateur.DateNaissance);

                        // Ex�cuter la commande d'insertion
                        int rowsAffected = command.ExecuteNonQuery();

                        if (rowsAffected > 0)
                        {
                            // L'insertion a r�ussi
                            successMessage = "Votre compte a �t� cr�� avec succ�s !";
                        }
                        else
                        {
                            // L'insertion a �chou�
                            errorMessage = "Une erreur s'est produite lors de la cr�ation du compte.";
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }
            utilisateur.Nom = "";
            utilisateur.Prenom = "";
            utilisateur.AdresseEmail = "";
            utilisateur.MotDePasse = "";
            utilisateur.DateNaissance = DateTime.MinValue;
            successMessage = "Votre compte a �t� cr�� avec succ�s.";
            Response.Redirect("/Login");
        }
    }
}
