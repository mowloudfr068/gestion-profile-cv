using GestionProfilCV.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Data.SqlClient;

namespace GestionProfilCV.Pages.Formations
{
    public class CreateModel : PageModel
    {
        public string errorMessage = "";
        public string successMessage = "";
        public Formation formation = new Formation();

        public void OnGet()
        {
        }

        public void OnPost()
        {
            int? idUtilisateur = HttpContext.Session.GetInt32("ID_Utilisateur");
            if (idUtilisateur == null)
            {
                return; // Ou redirigez vers une autre page, car aucun ID utilisateur n'est disponible dans la session
            }

            int id = (int)idUtilisateur;
            formation.ID_Utilisateur = id;
            formation.DiplomeOuCertificat = Request.Form["DiplomeOuCertificat"];
            formation.Institution = Request.Form["Institution"];
            formation.DomaineEtudes = Request.Form["DomaineEtudes"];

            string DateDebutString = Request.Form["DateDebut"];
            string DateFinString = Request.Form["DateFin"];
            DateTime dateDebut,dateFin;
            if (!DateTime.TryParse(DateDebutString, out dateDebut) || !DateTime.TryParse(DateFinString, out dateFin))
            {
                errorMessage = "La date de naissance est invalide.";
                return;
            }
            formation.DateDebut = dateDebut;
            formation.DateFin = dateFin;

            if (string.IsNullOrEmpty(formation.DiplomeOuCertificat)
                || string.IsNullOrEmpty(formation.Institution)
                || string.IsNullOrEmpty(formation.DomaineEtudes)
                || formation.DateDebut == DateTime.MinValue || formation.DateFin == DateTime.MinValue)
            {
                errorMessage = "Tous les champs doivent �tre remplis.";
                return;
            }

            // Le formulaire est valide, proc�der � la cr�ation du compte formation
            // ...

            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Cr�er la commande SQL pour l'insertion
                    string query = "INSERT INTO Formations (ID_Utilisateur, DiplomeOuCertificat, Institution, DomaineEtudes,DateDebut,DateFin) " +
                                   "VALUES (@ID_Utilisateur, @DiplomeOuCertificat, @Institution, @DomaineEtudes, @DateDebut,@DateFin)";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        // Param�tres de la commande
                        command.Parameters.AddWithValue("@ID_Utilisateur", formation.ID_Utilisateur);
                        command.Parameters.AddWithValue("@DiplomeOuCertificat", formation.DiplomeOuCertificat);
                        command.Parameters.AddWithValue("@Institution", formation.Institution);
                        command.Parameters.AddWithValue("@DomaineEtudes", formation.DomaineEtudes);
                        command.Parameters.AddWithValue("@DateDebut", formation.DateDebut);
                        command.Parameters.AddWithValue("@DateFin", formation.DateFin);

                        // Ex�cuter la commande d'insertion
                        int rowsAffected = command.ExecuteNonQuery();

                        if (rowsAffected > 0)
                        {
                            // L'insertion a r�ussi
                            successMessage = "Votre formation a �t� cr�� avec succ�s !";
                        }
                        else
                        {
                            // L'insertion a �chou�
                            errorMessage = "Une erreur s'est produite lors de la cr�ation du formation.";
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }
            formation.DiplomeOuCertificat = "";
            formation.Institution = "";
            formation.DomaineEtudes = "";
            formation.DateDebut = DateTime.MinValue;
            formation.DateFin = DateTime.MinValue;
            successMessage = "Votre formation a �t� cr�� avec succ�s.";
            Response.Redirect("/Formations/Create");
        }
    }
}
