using GestionProfilCV.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Data;
using System.Data.SqlClient;

namespace GestionProfilCV.Pages.Formations
{

    public class IndexModel : PageModel
    {
       public List<Formation> listFormations = new List<Formation>();
        public string errorMessage = "";
        public string successMessage = "";

        public void OnGet()
        {
            int? idUtilisateur = HttpContext.Session.GetInt32("ID_Utilisateur");
            if (idUtilisateur == null)
            {
                return; // Ou redirigez vers une autre page, car aucun ID utilisateur n'est disponible dans la session
            }

            int id = (int)idUtilisateur;
            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Cr�er la commande SQL pour l'insertion
                    string query = "SELECT * FROM Formations where ID_Utilisateur=@id";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        command.Parameters.AddWithValue("@id", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Formation formation = new Formation();
                                formation.ID_Formation = reader.GetInt32("ID_Formation");
                                formation.DiplomeOuCertificat = reader.GetString("DiplomeOuCertificat");
                                formation.Institution = reader.GetString("Institution");
                                formation.DomaineEtudes = reader.GetString("DomaineEtudes");
                                formation.DateDebut = reader.GetDateTime("DateDebut");
                                formation.DateFin = reader.GetDateTime("DateFin");
                                listFormations.Add(formation);
                            }
                        }


                    }

                }

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }
        }
    }
}
