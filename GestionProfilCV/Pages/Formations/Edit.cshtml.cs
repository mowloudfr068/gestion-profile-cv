using GestionProfilCV.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.VisualBasic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace GestionProfilCV.Pages.Formations
{
    public class EditModel : PageModel
    {
        public Formation formation = new Formation();
        public string errorMessage = "";
        public string successMessage = "";
        public void OnGet()
        {
            int? idUtilisateur = HttpContext.Session.GetInt32("ID_Utilisateur");
            int ID_Formation = Convert.ToInt32(Request.Query["ID_Formation"]);
            if (idUtilisateur == null)
            {
                return; // Ou redirigez vers une autre page, car aucun ID utilisateur n'est disponible dans la session
            }

            int id = (int)idUtilisateur;
            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Cr�er la commande SQL pour l'insertion
                    string query = "SELECT * FROM Formations where ID_Formation=@ID_Formation AND ID_Utilisateur=@id";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        // Param�tres de la commande
                        command.Parameters.AddWithValue("@ID_Formation", ID_Formation);
                        command.Parameters.AddWithValue("@id", id);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                formation.ID_Utilisateur = id;
                                formation.ID_Formation = reader.GetInt32("ID_Formation");
                                formation.DiplomeOuCertificat = reader.GetString("DiplomeOuCertificat");
                                formation.Institution = reader.GetString("Institution");
                                formation.DomaineEtudes = reader.GetString("DomaineEtudes");
                                formation.DateDebut = reader.GetDateTime("DateDebut");
                                formation.DateFin = reader.GetDateTime("DateFin");
                            }
                        }


                    }

                }

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }


        }

        public void OnPost()
        {
            formation.ID_Formation = Convert.ToInt32(Request.Form["ID_Formation"]);
            formation.DiplomeOuCertificat = Request.Form["DiplomeOuCertificat"];
            formation.Institution = Request.Form["Institution"];
            formation.DomaineEtudes = Request.Form["DomaineEtudes"];
            string DateDebutString = Request.Form["DateDebut"];
            string DateFinString = Request.Form["DateFin"];
            DateTime dateDebut, dateFin;
            if (!DateTime.TryParse(DateDebutString, out dateDebut) || !DateTime.TryParse(DateFinString, out dateFin))
            {
                errorMessage = "La date de naissance est invalide.";
                return;
            }
            formation.DateDebut = dateDebut;
            formation.DateFin = dateFin;

            if (string.IsNullOrEmpty(formation.DiplomeOuCertificat)
                || string.IsNullOrEmpty(formation.Institution)
                || string.IsNullOrEmpty(formation.DomaineEtudes)
                || formation.DateDebut == DateTime.MinValue || formation.DateFin == DateTime.MinValue)
            {
                errorMessage = "Tous les champs doivent �tre remplis.";
                return;
            }

            // Le formulaire est valide, proc�der � la cr�ation du compte formation
            // ...

            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    string query = "UPDATE Formations SET DiplomeOuCertificat = @DiplomeOuCertificat, Institution = @Institution, DomaineEtudes = @DomaineEtudes, DateDebut = @DateDebut, DateFin = @DateFin WHERE ID_Formation = @Id AND ID_Utilisateur=@ID_Utilisateur";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        command.Parameters.AddWithValue("@DiplomeOuCertificat", formation.DiplomeOuCertificat);
                        command.Parameters.AddWithValue("@Institution", formation.Institution);
                        command.Parameters.AddWithValue("@DomaineEtudes", formation.DomaineEtudes);
                        command.Parameters.AddWithValue("@DateDebut", formation.DateDebut);
                        command.Parameters.AddWithValue("@DateFin", formation.DateFin);
                        command.Parameters.AddWithValue("@Id", formation.ID_Formation);
                        command.Parameters.AddWithValue("@ID_Utilisateur", HttpContext.Session.GetInt32("ID_Utilisateur"));
                        command.ExecuteNonQuery();
                        Response.Redirect("/Formations/Index");
                    }
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }

        }
    }
}
