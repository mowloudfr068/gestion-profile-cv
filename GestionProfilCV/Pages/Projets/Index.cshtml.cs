using GestionProfilCV.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Data;
using System.Data.SqlClient;

namespace GestionProfilCV.Pages.Projets
{
    public class IndexModel : PageModel
    {
        public List<Projet> listProjets = new List<Projet>();
        public string errorMessage = "";
        public string successMessage = "";

        public void OnGet()
        {
            int? idUtilisateur = HttpContext.Session.GetInt32("ID_Utilisateur");
            if (idUtilisateur == null)
            {
                return; // Ou redirigez vers une autre page, car aucun ID utilisateur n'est disponible dans la session
            }

            int id = (int)idUtilisateur;
            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Cr�er la commande SQL pour l'insertion
                    string query = "SELECT * FROM Projets where ID_Utilisateur=@id";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        command.Parameters.AddWithValue("@id", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Projet projet = new Projet();
                                projet.ID_Projet = reader.GetInt32("ID_Projet");
                                projet.NomProjet = reader.GetString("NomProjet");
                                projet.DescriptionProjet = reader.GetString("DescriptionProjet");
                                projet.DateDebut = reader.GetDateTime("DateDebut");
                                projet.DateFin = reader.GetDateTime("DateFin");
                                listProjets.Add(projet);
                            }
                        }


                    }

                }

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }
        }
    }
}
