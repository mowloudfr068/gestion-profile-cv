using GestionProfilCV.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Data;
using System.Data.SqlClient;

namespace GestionProfilCV.Pages.Projets
{
    public class EditModel : PageModel
    {
        public Projet projet = new Projet();
        public string errorMessage = "";
        public string successMessage = "";
        public void OnGet()
        {
            int? idUtilisateur = HttpContext.Session.GetInt32("ID_Utilisateur");
            int ID_Projet = Convert.ToInt32(Request.Query["ID_Projet"]);
            if (idUtilisateur == null)
            {
                return; // Ou redirigez vers une autre page, car aucun ID utilisateur n'est disponible dans la session
            }

            int id = (int)idUtilisateur;
            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Cr�er la commande SQL pour l'insertion
                    string query = "SELECT * FROM Projets where ID_Projet=@ID_Projet AND ID_Utilisateur=@id";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        // Param�tres de la commande
                        command.Parameters.AddWithValue("@ID_Projet", ID_Projet);
                        command.Parameters.AddWithValue("@id", id);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                projet.ID_Utilisateur = id;
                                projet.ID_Projet = reader.GetInt32("ID_Projet");
                                projet.NomProjet = reader.GetString("NomProjet");
                                projet.DescriptionProjet = reader.GetString("DescriptionProjet");
                                projet.DateDebut = reader.GetDateTime("DateDebut");
                                projet.DateFin = reader.GetDateTime("DateFin");
                            }
                        }


                    }

                }

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }


        }

        public void OnPost()
        {
            projet.ID_Projet = Convert.ToInt32(Request.Form["ID_Projet"]);
            projet.NomProjet = Request.Form["NomProjet"];
            projet.DescriptionProjet = Request.Form["DescriptionProjet"];
            string DateDebutString = Request.Form["DateDebut"];
            string DateFinString = Request.Form["DateFin"];
            DateTime dateDebut, dateFin;
            if (!DateTime.TryParse(DateDebutString, out dateDebut) || !DateTime.TryParse(DateFinString, out dateFin))
            {
                errorMessage = "La date de naissance est invalide.";
                return;
            }
            projet.DateDebut = dateDebut;
            projet.DateFin = dateFin;

            if (string.IsNullOrEmpty(projet.NomProjet)
                || string.IsNullOrEmpty(projet.DescriptionProjet)
                || projet.DateDebut == DateTime.MinValue || projet.DateFin == DateTime.MinValue)
            {
                errorMessage = "Tous les champs doivent �tre remplis.";
                return;
            }

            // Le formulaire est valide, proc�der � la cr�ation du compte projet
            // ...

            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    string query = "UPDATE Projets SET NomProjet = @NomProjet, DescriptionProjet = @DescriptionProjet,DateDebut = @DateDebut, DateFin = @DateFin WHERE ID_Projet = @Id AND ID_Utilisateur=@ID_Utilisateur";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        command.Parameters.AddWithValue("@NomProjet", projet.NomProjet);
                        command.Parameters.AddWithValue("@DescriptionProjet", projet.DescriptionProjet);
                        command.Parameters.AddWithValue("@DateDebut", projet.DateDebut);
                        command.Parameters.AddWithValue("@DateFin", projet.DateFin);
                        command.Parameters.AddWithValue("@Id", projet.ID_Projet);
                        command.Parameters.AddWithValue("@ID_Utilisateur", HttpContext.Session.GetInt32("ID_Utilisateur"));
                        command.ExecuteNonQuery();
                        Response.Redirect("/Projets/Index");
                    }
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }

        }
    }
}
