using GestionProfilCV.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Data.SqlClient;

namespace GestionProfilCV.Pages.Projets
{
    public class CreateModel : PageModel
    {
        public string errorMessage = "";
        public string successMessage = "";
        public Projet projet = new Projet();

        public void OnGet()
        {
        }

        public void OnPost()
        {
            int? idUtilisateur = HttpContext.Session.GetInt32("ID_Utilisateur");
            if (idUtilisateur == null)
            {
                return; // Ou redirigez vers une autre page, car aucun ID utilisateur n'est disponible dans la session
            }

            int id = (int)idUtilisateur;
            projet.ID_Utilisateur = id;
            projet.NomProjet = Request.Form["NomProjet"];
            projet.DescriptionProjet = Request.Form["DescriptionProjet"];


            string DateDebutString = Request.Form["DateDebut"];
            string DateFinString = Request.Form["DateFin"];
            DateTime dateDebut, dateFin;
            if (!DateTime.TryParse(DateDebutString, out dateDebut) || !DateTime.TryParse(DateFinString, out dateFin))
            {
                errorMessage = "La date de naissance est invalide.";
                return;
            }
            projet.DateDebut = dateDebut;
            projet.DateFin = dateFin;

            if (string.IsNullOrEmpty(projet.NomProjet)
                || string.IsNullOrEmpty(projet.DescriptionProjet)
                || projet.DateDebut == DateTime.MinValue || projet.DateFin == DateTime.MinValue)
            {
                errorMessage = "Tous les champs doivent �tre remplis.";
                return;
            }

            // Le formulaire est valide, proc�der � la cr�ation du compte projet
            // ...

            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Cr�er la commande SQL pour l'insertion
                    string query = "INSERT INTO Projets (ID_Utilisateur, NomProjet, DescriptionProjet,DateDebut,DateFin) " +
                                   "VALUES (@ID_Utilisateur, @NomProjet, @DescriptionProjet,@DateDebut,@DateFin)";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        // Param�tres de la commande
                        command.Parameters.AddWithValue("@ID_Utilisateur", projet.ID_Utilisateur);
                        command.Parameters.AddWithValue("@NomProjet", projet.NomProjet);
                        command.Parameters.AddWithValue("@DescriptionProjet", projet.DescriptionProjet);
                        command.Parameters.AddWithValue("@DateDebut", projet.DateDebut);
                        command.Parameters.AddWithValue("@DateFin", projet.DateFin);

                        // Ex�cuter la commande d'insertion
                        int rowsAffected = command.ExecuteNonQuery();

                        if (rowsAffected > 0)
                        {
                            // L'insertion a r�ussi
                            successMessage = "Votre projet a �t� cr�� avec succ�s !";
                        }
                        else
                        {
                            // L'insertion a �chou�
                            errorMessage = "Une erreur s'est produite lors de la cr�ation du projet.";
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }
            projet.NomProjet = "";
            projet.DescriptionProjet = "";
            projet.DateDebut = DateTime.MinValue;
            projet.DateFin = DateTime.MinValue;
            successMessage = "Votre projet a �t� cr�� avec succ�s.";
            Response.Redirect("/Projets/Create");
        }
    }
}
