using GestionProfilCV.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Data.SqlClient;
using System.Linq;

namespace GestionProfilCV.Pages
{
    public class LoginModel : PageModel
    {
       

        public string successMessage = "";
        public string errorMessage = "";
        public string adresseEmail = "";
        public string motDePasse = "";


        public void OnGet()
        {

        }

        public void OnPost()
        {
            adresseEmail = Request.Form["AdresseEmail"];
            motDePasse = Request.Form["MotDePasse"];

            // V�rifier les informations de connexion dans la base de donn�es
            if (VerifyCredentials(adresseEmail, motDePasse))
            {
                // Les informations de connexion sont valides
                successMessage = "Connexion r�ussie !";

                // Rediriger vers la page d'accueil en conservant le nom de l'utilisateur dans la session
                HttpContext.Session.SetString("UtilisateurConnecte", GetUserName(adresseEmail, motDePasse));
                HttpContext.Session.SetInt32("ID_Utilisateur", GetUserId(adresseEmail, motDePasse));
                Response.Redirect("/Index");
            }
            else
            {
                // Les informations de connexion sont incorrectes
                errorMessage = "Identifiants invalides !";
            }
        }

        private bool VerifyCredentials(string adresseEmail, string motDePasse)
        {
            string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
            string query = "SELECT COUNT(*) FROM Utilisateurs WHERE AdresseEmail = @AdresseEmail AND MotDePasse = @MotDePasse";

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                using (SqlCommand command = new SqlCommand(query, con))
                {
                    command.Parameters.AddWithValue("@AdresseEmail", adresseEmail);
                    command.Parameters.AddWithValue("@MotDePasse", motDePasse);

                    int count = (int)command.ExecuteScalar();

                    return count > 0;
                }
            }
        }


        private string GetUserName(string adresseEmail, string motDePasse)
        {
            string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
            string query = "SELECT Nom FROM Utilisateurs WHERE AdresseEmail = @AdresseEmail AND MotDePasse = @MotDePasse";

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                using (SqlCommand command = new SqlCommand(query, con))
                {
                    command.Parameters.AddWithValue("@AdresseEmail", adresseEmail);
                    command.Parameters.AddWithValue("@MotDePasse", motDePasse);

                    string nomUtilisateur = (string)command.ExecuteScalar();

                    return nomUtilisateur;
                }
            }
        }
        private int GetUserId(string adresseEmail, string motDePasse)
        {
            string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
            string query = "SELECT ID_Utilisateur FROM Utilisateurs WHERE AdresseEmail = @AdresseEmail AND MotDePasse = @MotDePasse";

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                using (SqlCommand command = new SqlCommand(query, con))
                {
                    command.Parameters.AddWithValue("@AdresseEmail", adresseEmail);
                    command.Parameters.AddWithValue("@MotDePasse", motDePasse);

                    int idUtilisateur = (int)command.ExecuteScalar();

                    return idUtilisateur;
                }
            }
        }


    }
}
