using GestionProfilCV.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Data;
using System.Data.SqlClient;

namespace GestionProfilCV.Pages.ExperiencesProfessionnelles
{
    public class EditModel : PageModel
    {
        public ExperienceProfessionnelle experienceProfessionnelle = new ExperienceProfessionnelle();
        public string errorMessage = "";
        public string successMessage = "";
        public void OnGet()
        {
            int? idUtilisateur = HttpContext.Session.GetInt32("ID_Utilisateur");
            int ID_Experience = Convert.ToInt32(Request.Query["ID_Experience"]);
            if (idUtilisateur == null)
            {
                return; // Ou redirigez vers une autre page, car aucun ID utilisateur n'est disponible dans la session
            }

            int id = (int)idUtilisateur;
            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Cr�er la commande SQL pour l'insertion
                    string query = "SELECT * FROM ExperiencesProfessionnelles where ID_Experience=@ID_Experience AND ID_Utilisateur=@id";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        // Param�tres de la commande
                        command.Parameters.AddWithValue("@ID_Experience", ID_Experience);
                        command.Parameters.AddWithValue("@id", id);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                experienceProfessionnelle.ID_Utilisateur = id;
                                experienceProfessionnelle.ID_Experience = reader.GetInt32("ID_Experience");
                                experienceProfessionnelle.TitrePoste = reader.GetString("TitrePoste");
                                experienceProfessionnelle.Entreprise = reader.GetString("Entreprise");
                                experienceProfessionnelle.DescriptionTaches = reader.GetString("DescriptionTaches");
                                experienceProfessionnelle.DateDebut = reader.GetDateTime("DateDebut");
                                experienceProfessionnelle.DateFin = reader.GetDateTime("DateFin");
                            }
                        }


                    }

                }

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }


        }

        public void OnPost()
        {
            experienceProfessionnelle.ID_Experience = Convert.ToInt32(Request.Form["ID_Experience"]);
            experienceProfessionnelle.TitrePoste = Request.Form["TitrePoste"];
            experienceProfessionnelle.Entreprise = Request.Form["Entreprise"];
            experienceProfessionnelle.DescriptionTaches = Request.Form["DescriptionTaches"];
            string DateDebutString = Request.Form["DateDebut"];
            string DateFinString = Request.Form["DateFin"];
            DateTime dateDebut, dateFin;
            if (!DateTime.TryParse(DateDebutString, out dateDebut) || !DateTime.TryParse(DateFinString, out dateFin))
            {
                errorMessage = "La date de naissance est invalide.";
                return;
            }
            experienceProfessionnelle.DateDebut = dateDebut;
            experienceProfessionnelle.DateFin = dateFin;

            if (string.IsNullOrEmpty(experienceProfessionnelle.TitrePoste)
                || string.IsNullOrEmpty(experienceProfessionnelle.Entreprise)
                || string.IsNullOrEmpty(experienceProfessionnelle.DescriptionTaches)
                || experienceProfessionnelle.DateDebut == DateTime.MinValue || experienceProfessionnelle.DateFin == DateTime.MinValue)
            {
                errorMessage = "Tous les champs doivent �tre remplis.";
                return;
            }

            // Le formulaire est valide, proc�der � la cr�ation du compte formation
            // ...

            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string query = "UPDATE ExperiencesProfessionnelles SET TitrePoste = @TitrePoste, Entreprise = @Entreprise, DescriptionTaches = @DescriptionTaches, DateDebut = @DateDebut, DateFin = @DateFin WHERE ID_Experience = @Id AND ID_Utilisateur=@ID_Utilisateur";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        command.Parameters.AddWithValue("@TitrePoste", experienceProfessionnelle.TitrePoste);
                        command.Parameters.AddWithValue("@Entreprise", experienceProfessionnelle.Entreprise);
                        command.Parameters.AddWithValue("@DescriptionTaches", experienceProfessionnelle.DescriptionTaches);
                        command.Parameters.AddWithValue("@DateDebut", experienceProfessionnelle.DateDebut);
                        command.Parameters.AddWithValue("@DateFin", experienceProfessionnelle.DateFin);
                        command.Parameters.AddWithValue("@Id", experienceProfessionnelle.ID_Experience);
                        command.Parameters.AddWithValue("@ID_Utilisateur", HttpContext.Session.GetInt32("ID_Utilisateur"));
                        command.ExecuteNonQuery();
                        Response.Redirect("/ExperiencesProfessionnelles/Index");
                    }
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }

        }
    }
}
