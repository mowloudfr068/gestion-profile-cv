using GestionProfilCV.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Data.SqlClient;

namespace GestionProfilCV.Pages.ExperiencesProfessionnelles
{
    public class CreateModel : PageModel
    {
        public string errorMessage = "";
        public string successMessage = "";
        public ExperienceProfessionnelle experienceProfessionnelle= new ExperienceProfessionnelle();

        public void OnGet()
        {
        }

        public void OnPost()
        {
            int? idUtilisateur = HttpContext.Session.GetInt32("ID_Utilisateur");
            if (idUtilisateur == null)
            {
                return; // Ou redirigez vers une autre page, car aucun ID utilisateur n'est disponible dans la session
            }

            int id = (int)idUtilisateur;
            experienceProfessionnelle.ID_Utilisateur = id;
            experienceProfessionnelle.TitrePoste = Request.Form["TitrePoste"];
            experienceProfessionnelle.Entreprise = Request.Form["Entreprise"];
            experienceProfessionnelle.DescriptionTaches = Request.Form["DescriptionTaches"];
            string DateDebutString = Request.Form["DateDebut"];
            string DateFinString = Request.Form["DateFin"];
            DateTime dateDebut, dateFin;
            if (!DateTime.TryParse(DateDebutString, out dateDebut) || !DateTime.TryParse(DateFinString, out dateFin))
            {
                errorMessage = "La date de naissance est invalide.";
                return;
            }
            experienceProfessionnelle.DateDebut = dateDebut;
            experienceProfessionnelle.DateFin = dateFin;

            if (string.IsNullOrEmpty(experienceProfessionnelle.TitrePoste)
                || string.IsNullOrEmpty(experienceProfessionnelle.Entreprise)
                || string.IsNullOrEmpty(experienceProfessionnelle.DescriptionTaches)
                || experienceProfessionnelle.DateDebut == DateTime.MinValue || experienceProfessionnelle.DateFin == DateTime.MinValue)
            {
                errorMessage = "Tous les champs doivent �tre remplis.";
                return;
            }

            // Le formulaire est valide, proc�der � la cr�ation du compte formation
            // ...

            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Cr�er la commande SQL pour l'insertion
                    string query = "INSERT INTO ExperiencesProfessionnelles (ID_Utilisateur, TitrePoste, Entreprise, DescriptionTaches,DateDebut,DateFin) " +
                                   "VALUES (@ID_Utilisateur, @TitrePoste, @Entreprise, @DescriptionTaches, @DateDebut,@DateFin)";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        // Param�tres de la commande
                        command.Parameters.AddWithValue("@ID_Utilisateur", experienceProfessionnelle.ID_Utilisateur);
                        command.Parameters.AddWithValue("@TitrePoste", experienceProfessionnelle.TitrePoste);
                        command.Parameters.AddWithValue("@Entreprise", experienceProfessionnelle.Entreprise);
                        command.Parameters.AddWithValue("@DescriptionTaches", experienceProfessionnelle.DescriptionTaches);
                        command.Parameters.AddWithValue("@DateDebut", experienceProfessionnelle.DateDebut);
                        command.Parameters.AddWithValue("@DateFin", experienceProfessionnelle.DateFin);

                        // Ex�cuter la commande d'insertion
                        int rowsAffected = command.ExecuteNonQuery();

                        if (rowsAffected > 0)
                        {
                            // L'insertion a r�ussi
                            successMessage = "Votre Experiences a �t� cr�� avec succ�s !";
                        }
                        else
                        {
                            // L'insertion a �chou�
                            errorMessage = "Une erreur s'est produite lors de la cr�ation du ExperiencesProfessionnelles.";
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }
            experienceProfessionnelle.TitrePoste = "";
            experienceProfessionnelle.Entreprise = "";
            experienceProfessionnelle.DescriptionTaches = "";
            experienceProfessionnelle.DateDebut = DateTime.MinValue;
            experienceProfessionnelle.DateFin = DateTime.MinValue;
            successMessage = "Votre experience a �t� cr�� avec succ�s.";
            Response.Redirect("/ExperiencesProfessionnelles/Create");
        }
    }
}
