using GestionProfilCV.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Data;
using System.Data.SqlClient;

namespace GestionProfilCV.Pages
{
    public class DisplayCvModel : PageModel
    {
        public List<Formation> listFormations = new List<Formation>();
        public List<ExperienceProfessionnelle> listExperiencesProfessionnelles = new List<ExperienceProfessionnelle>();
        public List<Competence> listCompetences = new List<Competence>();
        public List<Projet> listProjets = new List<Projet>();
        public List<Certification> listCertifications = new List<Certification>();
        public Utilisateur utilisateur = new Utilisateur();
        public void OnGet()
        {
            int? idUtilisateur = HttpContext.Session.GetInt32("ID_Utilisateur");
            if (idUtilisateur == null)
            {
                return; // Ou redirigez vers une autre page, car aucun ID utilisateur n'est disponible dans la session
            }

            int id = (int)idUtilisateur;
            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Cr�er la commande SQL pour l'insertion
                    string query = "SELECT * FROM Utilisateurs where ID_Utilisateur=@id";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        // Param�tres de la commande
                        command.Parameters.AddWithValue("@id", id);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                utilisateur.ID_Utilisateur = id;
                                utilisateur.Nom = reader.GetString(1);
                                utilisateur.Prenom = reader.GetString(2);
                                utilisateur.AdresseEmail = reader.GetString(3);
                                utilisateur.MotDePasse = reader.GetString(4);
                                utilisateur.DateNaissance = reader.GetDateTime(5);
                            }
                        }


                    }

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("une erreur produit" + ex);
            }
            FormationsCount(id);
            ExperiencesCount(id);
            CompetencesCount(id);
            ProjetsCount(id);
            CertificationsCount(id);


        }

        private void FormationsCount(int id)
        {
            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Cr�er la commande SQL pour l'insertion
                    string query = "SELECT * FROM Formations where ID_Utilisateur=@id";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        command.Parameters.AddWithValue("@id", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Formation formation = new Formation();
                                formation.ID_Formation = reader.GetInt32("ID_Formation");
                                formation.DiplomeOuCertificat = reader.GetString("DiplomeOuCertificat");
                                formation.Institution = reader.GetString("Institution");
                                formation.DomaineEtudes = reader.GetString("DomaineEtudes");
                                formation.DateDebut = reader.GetDateTime("DateDebut");
                                formation.DateFin = reader.GetDateTime("DateFin");
                                listFormations.Add(formation);
                            }
                        }


                    }

                }

            }
            catch (Exception ex)
            {

                Console.WriteLine("une erreur ce produit !");
            }
        }
        private void ExperiencesCount(int id)
        {
            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Cr�er la commande SQL pour l'insertion
                    string query = "SELECT * FROM ExperiencesProfessionnelles where ID_Utilisateur=@id";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        command.Parameters.AddWithValue("@id", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ExperienceProfessionnelle experienceProfessionnelle = new ExperienceProfessionnelle();
                                experienceProfessionnelle.ID_Experience = reader.GetInt32("ID_Experience");
                                experienceProfessionnelle.TitrePoste = reader.GetString("TitrePoste");
                                experienceProfessionnelle.Entreprise = reader.GetString("Entreprise");
                                experienceProfessionnelle.DescriptionTaches = reader.GetString("DescriptionTaches");
                                experienceProfessionnelle.DateDebut = reader.GetDateTime("DateDebut");
                                experienceProfessionnelle.DateFin = reader.GetDateTime("DateFin");
                                listExperiencesProfessionnelles.Add(experienceProfessionnelle);
                            }
                        }


                    }

                }

            }
            catch (Exception ex)
            {

                Console.WriteLine("une erreur ce produit !" + ex);
            }
        }

        private void CompetencesCount(int id)
        {
            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Cr�er la commande SQL pour l'insertion
                    string query = "SELECT * FROM Competences where ID_Utilisateur=@id";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        command.Parameters.AddWithValue("@id", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Competence competence = new Competence();
                                competence.ID_Competence = reader.GetInt32("ID_Competence");
                                competence.NomCompetence = reader.GetString("NomCompetence");
                                competence.NiveauCompetence = reader.GetString("NiveauCompetence");
                                listCompetences.Add(competence);
                            }
                        }


                    }

                }

            }
            catch (Exception ex)
            {

                Console.WriteLine("une erreur ce produit !" + ex);
            }
        }

        private void ProjetsCount(int id)
        {
            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Cr�er la commande SQL pour l'insertion
                    string query = "SELECT * FROM Projets where ID_Utilisateur=@id";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        command.Parameters.AddWithValue("@id", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Projet projet = new Projet();
                                projet.ID_Projet = reader.GetInt32("ID_Projet");
                                projet.NomProjet = reader.GetString("NomProjet");
                                projet.DescriptionProjet = reader.GetString("DescriptionProjet");
                                projet.DateDebut = reader.GetDateTime("DateDebut");
                                projet.DateFin = reader.GetDateTime("DateFin");
                                listProjets.Add(projet);
                            }
                        }


                    }

                }

            }
            catch (Exception ex)
            {

                Console.WriteLine("une erreur ce produit !" + ex);
            }
        }

        private void CertificationsCount(int id)
        {
            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Cr�er la commande SQL pour l'insertion
                    string query = "SELECT * FROM Certifications where ID_Utilisateur=@id";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        command.Parameters.AddWithValue("@id", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Certification certification = new Certification();
                                certification.ID_Certification = reader.GetInt32("ID_Certification");
                                certification.NomCertification = reader.GetString("NomCertification");
                                certification.AutoriteCertification = reader.GetString("AutoriteCertification");
                                certification.DateObtention = reader.GetDateTime("DateObtention");
                                listCertifications.Add(certification);
                            }
                        }


                    }

                }

            }
            catch (Exception ex)
            {

                Console.WriteLine("une erreur ce produit !" + ex);
            }
        }


        public IActionResult OnPost()
        {
			int? idUtilisateur = HttpContext.Session.GetInt32("ID_Utilisateur");
			if (idUtilisateur == null)
			{
                Console.WriteLine("une erreur qui a produit"); // Ou redirigez vers une autre page, car aucun ID utilisateur n'est disponible dans la session
			}

			int id = (int)idUtilisateur;
			try
			{
				string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
				using (SqlConnection con = new SqlConnection(connectionString))
				{

					con.Open();
					// Cr�er la commande SQL pour l'insertion
					string query = "SELECT * FROM Utilisateurs where ID_Utilisateur=@id";
					using (SqlCommand command = new SqlCommand(query, con))
					{
						// Param�tres de la commande
						command.Parameters.AddWithValue("@id", id);

						using (SqlDataReader reader = command.ExecuteReader())
						{
							if (reader.Read())
							{
								utilisateur.ID_Utilisateur = id;
								utilisateur.Nom = reader.GetString(1);
								utilisateur.Prenom = reader.GetString(2);
								utilisateur.AdresseEmail = reader.GetString(3);
								utilisateur.MotDePasse = reader.GetString(4);
								utilisateur.DateNaissance = reader.GetDateTime(5);
							}
						}


					}

				}

			}
			catch (Exception ex)
			{
				Console.WriteLine("une erreur produit" + ex);
			}
			FormationsCount(id);
			ExperiencesCount(id);
			CompetencesCount(id);
			ProjetsCount(id);
			CertificationsCount(id);

			// G�n�rer le contenu du PDF
			byte[] pdfBytes = GeneratePDF();

			// T�l�charger le PDF
			return File(pdfBytes, "application/pdf", "cv.pdf");
        }

        private byte[] GeneratePDF()
        {
            using (MemoryStream ms = new MemoryStream())
            {
                Document document = new Document();
                PdfWriter writer = PdfWriter.GetInstance(document, ms);

                document.Open();

                // Ajouter les informations de l'utilisateur
                Paragraph userInformation = new Paragraph();
                userInformation.Add(new Phrase($"Nom : {utilisateur.Nom}\n"));
                userInformation.Add(new Phrase($"Pr�nom : {utilisateur.Prenom}\n"));
                userInformation.Add(new Phrase($"Email : {utilisateur.AdresseEmail}\n"));
                userInformation.Add(new Phrase($"Date de naissance : {utilisateur.DateNaissance.ToShortDateString()}\n\n"));
                document.Add(userInformation);

                // Ajouter les exp�riences professionnelles
                if (listExperiencesProfessionnelles.Any())
                {
                    Paragraph experiencesTitle = new Paragraph("Exp�riences professionnelles", new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, BaseColor.GRAY));
                    document.Add(experiencesTitle);

                    foreach (var experience in listExperiencesProfessionnelles)
                    {
                        Paragraph experienceInfo = new Paragraph();
                        experienceInfo.Add(new Phrase($"Titre du poste : {experience.TitrePoste}\n"));
                        experienceInfo.Add(new Phrase($"Entreprise : {experience.Entreprise}\n"));
                        experienceInfo.Add(new Phrase($"Date de d�but : {experience.DateDebut.ToShortDateString()}\n"));
                        experienceInfo.Add(new Phrase($"Date de fin : {experience.DateFin.ToShortDateString()}\n"));
                        experienceInfo.Add(new Phrase($"Description des t�ches : {experience.DescriptionTaches}\n\n"));
                        document.Add(experienceInfo);
                    }
                }

                // Ajouter les formations
                if (listFormations.Any())
                {
                    Paragraph formationsTitle = new Paragraph("Formations", new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, BaseColor.GRAY));
                    document.Add(formationsTitle);

                    foreach (var formation in listFormations)
                    {
                        Paragraph formationInfo = new Paragraph();
                        formationInfo.Add(new Phrase($"Dipl�me ou certificat : {formation.DiplomeOuCertificat}\n"));
                        formationInfo.Add(new Phrase($"Institution : {formation.Institution}\n"));
                        formationInfo.Add(new Phrase($"Date de d�but : {formation.DateDebut.ToShortDateString()}\n"));
                        formationInfo.Add(new Phrase($"Date de fin : {formation.DateFin.ToShortDateString()}\n"));
                        formationInfo.Add(new Phrase($"Domaine d'�tudes : {formation.DomaineEtudes}\n\n"));
                        document.Add(formationInfo);
                    }
                }

                // Ajouter les comp�tences
                if (listCompetences.Any())
                {
                    Paragraph competencesTitle = new Paragraph("Comp�tences", new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, BaseColor.GRAY));
                    document.Add(competencesTitle);

                    foreach (var competence in listCompetences)
                    {
                        Paragraph competenceInfo = new Paragraph();
                        competenceInfo.Add(new Phrase($"Nom de la comp�tence : {competence.NomCompetence}\n"));
                        competenceInfo.Add(new Phrase($"Niveau de comp�tence : {competence.NiveauCompetence}\n\n"));
                        document.Add(competenceInfo);
                    }
                }

                // Ajouter les projets
                if (listProjets.Any())
                {
                    Paragraph projetsTitle = new Paragraph("Projets", new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, BaseColor.GRAY));
                    document.Add(projetsTitle);

                    foreach (var projet in listProjets)
                    {
                        Paragraph projetInfo = new Paragraph();
                        projetInfo.Add(new Phrase($"Nom du projet : {projet.NomProjet}\n"));
                        projetInfo.Add(new Phrase($"Description du projet : {projet.DescriptionProjet}\n"));
                        projetInfo.Add(new Phrase($"Date de d�but : {projet.DateDebut.ToShortDateString()}\n"));
                        projetInfo.Add(new Phrase($"Date de fin : {projet.DateFin.ToShortDateString()}\n\n"));
                        document.Add(projetInfo);
                    }
                }

                // Ajouter les certifications
                if (listCertifications.Any())
                {
                    Paragraph certificationsTitle = new Paragraph("Certifications", new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, BaseColor.GRAY));
                    document.Add(certificationsTitle);

                    foreach (var certification in listCertifications)
                    {
                        Paragraph certificationInfo = new Paragraph();
                        certificationInfo.Add(new Phrase($"Nom de la certification : {certification.NomCertification}\n"));
                        certificationInfo.Add(new Phrase($"Autorit� de certification : {certification.AutoriteCertification}\n"));
                        certificationInfo.Add(new Phrase($"Date d'obtention : {certification.DateObtention.ToShortDateString()}\n\n"));
                        document.Add(certificationInfo);
                    }
                }

                document.Close();
                writer.Close();

                return ms.ToArray();
            }
        }
    }

	
}
