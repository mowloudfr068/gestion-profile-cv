﻿using GestionProfilCV.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;

namespace GestionProfilCV.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }
        public List<Formation> listFormations = new List<Formation>();
        public List<ExperienceProfessionnelle> listExperiencesProfessionnelles = new List<ExperienceProfessionnelle>();
        public List<Competence> listCompetences = new List<Competence>();
        public List<Projet> listProjets = new List<Projet>();
        public List<Certification> listCertifications = new List<Certification>();
        public void OnGet()
        {
            int? idUtilisateur = HttpContext.Session.GetInt32("ID_Utilisateur");
            if (idUtilisateur == null)
            {
                return; // Ou redirigez vers une autre page, car aucun ID utilisateur n'est disponible dans la session
            }

            int id = (int)idUtilisateur;
            FormationsCount(id);
            ExperiencesCount(id);
            CompetencesCount(id);
            ProjetsCount(id);
            CertificationsCount(id);


        }

        private void FormationsCount(int id)
        {
            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Créer la commande SQL pour l'insertion
                    string query = "SELECT * FROM Formations where ID_Utilisateur=@id";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        command.Parameters.AddWithValue("@id", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Formation formation = new Formation();
                                formation.ID_Formation = reader.GetInt32("ID_Formation");
                                formation.DiplomeOuCertificat = reader.GetString("DiplomeOuCertificat");
                                formation.Institution = reader.GetString("Institution");
                                formation.DomaineEtudes = reader.GetString("DomaineEtudes");
                                formation.DateDebut = reader.GetDateTime("DateDebut");
                                formation.DateFin = reader.GetDateTime("DateFin");
                                listFormations.Add(formation);
                            }
                        }

                        
                    }

                }

            }
            catch (Exception ex)
            {

                Console.WriteLine("une erreur ce produit !");
            }
        }
        private void ExperiencesCount(int id)
        {
            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Créer la commande SQL pour l'insertion
                    string query = "SELECT * FROM ExperiencesProfessionnelles where ID_Utilisateur=@id";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        command.Parameters.AddWithValue("@id", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ExperienceProfessionnelle experienceProfessionnelle = new ExperienceProfessionnelle();
                                experienceProfessionnelle.ID_Experience = reader.GetInt32("ID_Experience");
                                experienceProfessionnelle.TitrePoste = reader.GetString("TitrePoste");
                                experienceProfessionnelle.Entreprise = reader.GetString("Entreprise");
                                experienceProfessionnelle.DescriptionTaches = reader.GetString("DescriptionTaches");
                                experienceProfessionnelle.DateDebut = reader.GetDateTime("DateDebut");
                                experienceProfessionnelle.DateFin = reader.GetDateTime("DateFin");
                                listExperiencesProfessionnelles.Add(experienceProfessionnelle);
                            }
                        }


                    }

                }

            }
            catch (Exception ex)
            {

                Console.WriteLine("une erreur ce produit !"+ex);
            }
        }

        private void CompetencesCount(int id)
        {
            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Créer la commande SQL pour l'insertion
                    string query = "SELECT * FROM Competences where ID_Utilisateur=@id";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        command.Parameters.AddWithValue("@id", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Competence competence = new Competence();
                                competence.ID_Competence = reader.GetInt32("ID_Competence");
                                competence.NomCompetence = reader.GetString("NomCompetence");
                                competence.NiveauCompetence = reader.GetString("NiveauCompetence");
                                listCompetences.Add(competence);
                            }
                        }


                    }

                }

            }
            catch (Exception ex)
            {

                Console.WriteLine("une erreur ce produit !"+ex);
            }
        }

        private void ProjetsCount(int id)
        {
            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Créer la commande SQL pour l'insertion
                    string query = "SELECT * FROM Projets where ID_Utilisateur=@id";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        command.Parameters.AddWithValue("@id", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Projet projet = new Projet();
                                projet.ID_Projet = reader.GetInt32("ID_Projet");
                                projet.NomProjet = reader.GetString("NomProjet");
                                projet.DescriptionProjet = reader.GetString("DescriptionProjet");
                                projet.DateDebut = reader.GetDateTime("DateDebut");
                                projet.DateFin = reader.GetDateTime("DateFin");
                                listProjets.Add(projet);
                            }
                        }


                    }

                }

            }
            catch (Exception ex)
            {

                Console.WriteLine("une erreur ce produit !" + ex);
            }
        }

        private void CertificationsCount(int id)
        {
            try
            {
                string connectionString = "Data Source=localhost;Initial Catalog=GestionProfilCV;Integrated Security=True;";
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    con.Open();
                    // Créer la commande SQL pour l'insertion
                    string query = "SELECT * FROM Certifications where ID_Utilisateur=@id";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        command.Parameters.AddWithValue("@id", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Certification certification = new Certification();
                                certification.ID_Certification = reader.GetInt32("ID_Certification");
                                certification.NomCertification = reader.GetString("NomCertification");
                                certification.AutoriteCertification = reader.GetString("AutoriteCertification");
                                certification.DateObtention = reader.GetDateTime("DateObtention");
                                listCertifications.Add(certification);
                            }
                        }


                    }

                }

            }
            catch (Exception ex)
            {

                Console.WriteLine("une erreur ce produit !" + ex);
            }
        }




    }
}