-- Table Utilisateurs
CREATE TABLE Utilisateurs (
  ID_Utilisateur INT IDENTITY(1,1) PRIMARY KEY,
  Nom VARCHAR(50),
  Prenom VARCHAR(50),
  AdresseEmail VARCHAR(100),
  MotDePasse VARCHAR(255),
  DateNaissance DATE,
  -- Autres informations utilisateur
  -- ...
);

-- Table ExperiencesProfessionnelles
CREATE TABLE ExperiencesProfessionnelles (
  ID_Experience INT IDENTITY(1,1) PRIMARY KEY,
  ID_Utilisateur INT,
  TitrePoste VARCHAR(100),
  Entreprise VARCHAR(100),
  DescriptionTaches TEXT,
  DateDebut DATE,
  DateFin DATE,
  FOREIGN KEY (ID_Utilisateur) REFERENCES Utilisateurs(ID_Utilisateur)
);

-- Table Formations
CREATE TABLE Formations (
  ID_Formation INT IDENTITY(1,1) PRIMARY KEY,
  ID_Utilisateur INT,
  DiplomeOuCertificat VARCHAR(100),
  Institution VARCHAR(100),
  DomaineEtudes VARCHAR(100),
  DateDebut DATE,
  DateFin DATE,
  FOREIGN KEY (ID_Utilisateur) REFERENCES Utilisateurs(ID_Utilisateur)
);

-- Table Competences
CREATE TABLE Competences (
  ID_Competence INT IDENTITY(1,1) PRIMARY KEY,
  ID_Utilisateur INT,
  NomCompetence VARCHAR(100),
  NiveauCompetence VARCHAR(50),
  FOREIGN KEY (ID_Utilisateur) REFERENCES Utilisateurs(ID_Utilisateur)
);

-- Table Projets
CREATE TABLE Projets (
  ID_Projet INT IDENTITY(1,1) PRIMARY KEY,
  ID_Utilisateur INT,
  NomProjet VARCHAR(100),
  DescriptionProjet TEXT,
  DateDebut DATE,
  DateFin DATE,
  FOREIGN KEY (ID_Utilisateur) REFERENCES Utilisateurs(ID_Utilisateur)
);

-- Table Certifications
CREATE TABLE Certifications (
  ID_Certification INT IDENTITY(1,1) PRIMARY KEY,
  ID_Utilisateur INT,
  NomCertification VARCHAR(100),
  AutoriteCertification VARCHAR(100),
  DateObtention DATE,
  FOREIGN KEY (ID_Utilisateur) REFERENCES Utilisateurs(ID_Utilisateur)
);
