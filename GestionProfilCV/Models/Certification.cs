﻿namespace GestionProfilCV.Models
{
    public class Certification
    {
        public int ID_Certification { get; set; }
        public int ID_Utilisateur { get; set; }
        public string NomCertification { get; set; }
        public string AutoriteCertification { get; set; }
        public DateTime DateObtention { get; set; }

        public Utilisateur Utilisateur { get; set; }
    }

}
