﻿namespace GestionProfilCV.Models
{
    public class ExperienceProfessionnelle
    {
        public int ID_Experience { get; set; }
        public int ID_Utilisateur { get; set; }
        public string TitrePoste { get; set; }
        public string Entreprise { get; set; }
        public string DescriptionTaches { get; set; }
        public DateTime DateDebut { get; set; }
        public DateTime DateFin { get; set; }

        public Utilisateur Utilisateur { get; set; }
    }

}
