﻿namespace GestionProfilCV.Models
{
    public class Formation
    {
        public int ID_Formation { get; set; }
        public int ID_Utilisateur { get; set; }
        public string DiplomeOuCertificat { get; set; }
        public string Institution { get; set; }
        public string DomaineEtudes { get; set; }
        public DateTime DateDebut { get; set; }
        public DateTime DateFin { get; set; }

        public Utilisateur Utilisateur { get; set; }
    }

}
