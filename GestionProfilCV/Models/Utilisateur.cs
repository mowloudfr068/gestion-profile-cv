﻿
namespace GestionProfilCV.Models
{
    public class Utilisateur
    {
            public int ID_Utilisateur { get; set; }
            public string Nom { get; set; }
            public string Prenom { get; set; }
            public string AdresseEmail { get; set; }
            public string MotDePasse { get; set; }
            public DateTime DateNaissance { get; set; }
    }

}
