﻿namespace GestionProfilCV.Models
{
    public class Competence
    {
        public int ID_Competence { get; set; }
        public int ID_Utilisateur { get; set; }
        public string NomCompetence { get; set; }
        public string NiveauCompetence { get; set; }

        public Utilisateur Utilisateur { get; set; }
    }

}
