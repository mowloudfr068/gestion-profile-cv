﻿namespace GestionProfilCV.Models
{
    public class Projet
    {
        public int ID_Projet { get; set; }
        public int ID_Utilisateur { get; set; }
        public string NomProjet { get; set; }
        public string DescriptionProjet { get; set; }
        public DateTime DateDebut { get; set; }
        public DateTime DateFin { get; set; }

        public Utilisateur Utilisateur { get; set; }
    }

}
